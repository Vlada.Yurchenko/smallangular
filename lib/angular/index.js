import * as baseDirectives from './baseDirectives';
import * as baseFilters from './filters';
import { toCamelCase, getAllAttributes, getDependencyNames } from './utils';
import { timeout, http } from './services';
import pubSub from './pubSub';

function SmallAngular() {
  const $rootScope = window;
  const directives = { ...baseDirectives };
  const filters = { ...baseFilters };
  const components = {};
  const controllers = {};
  const $services = {};
  const $config = [];
  let watchers = [];

  Object.assign($rootScope.__proto__, pubSub); // eslint-disable-line no-proto

  function directive(name, fn) {
    directives[name] = fn;

    return this;
  }

  function filter(name, fn) {
    directives[name] = fn;

    return this;
  }

  function setDependencies(args) {
    const fn = args.call ? args : args[args.length - 1];
    fn.dependencies = getDependencyNames(args);

    return fn;
  }

  function component(name, args) {
    const fn = setDependencies(args);
    components[name] = fn;

    return this;
  }

  function controller(name, args) {
    const fn = setDependencies(args);
    controllers[name] = fn;

    return this;
  }

  const service = (name, args) => {
    const fn = setDependencies(args);
    const injections = fn.dependencies.map(name => this[name] || $services[name]);
    $services[name] = fn(...injections);

    return this;
  };

  function config(args) {
    const fn = setDependencies(args);
    $config.push(fn);

    return this;
  }

  const constant = (name, constant) => {
    $services[name] = constant;
  };

  this.createApp = appName => {
    this.appName = appName;

    return this;
  };

  const compile = node => {
    const [allAttr, htmlAttr] = getAllAttributes(node);
    const tagName = toCamelCase(node.tagName);

    if (components[tagName]) {
      const component = components[tagName];
      const componentInjections = component.dependencies.map(name => this[name] || $services[name]);
      const { controller, controllerAs, link, template } = component(...componentInjections);

      if (template) {
        node.innerHTML = template;
      }

      if (!controller && !link) {
        return;
      }

      if (!controller && link) {
        return link($rootScope, node, htmlAttr);
      }

      const ctrl = controllers[controller];
      const ctrlInjections = ctrl.dependencies.map(name => this[name] || $services[name]);

      if (controllerAs) {
        $rootScope[controllerAs] = new controllers[controller](...ctrlInjections);
      }

      link($rootScope, node, htmlAttr, ctrl);

      return;
    }

    allAttr.forEach(attr => {
      const attrName = toCamelCase(attr);

      if (directives[attrName]) {
        const directive = directives[attrName];
        const dependencies = getDependencyNames(directive);

        directive(...dependencies).link($rootScope, node, htmlAttr);
      }
    });
  };

  $rootScope.$$compile = compile;

  $rootScope.$apply = handler => {
    if (handler) {
      handler();
    }

    watchers = watchers.filter(({ node: { isConnected }, cb }) => {
      if (isConnected) {
        cb();
      }

      return isConnected;
    });
  };

  $rootScope.$applyAsync = handler => {
    setTimeout(() => $rootScope.$apply(handler), 0);
  };

  $rootScope.$whatch = (_, node, cb) => {
    watchers.push({ node, cb });
  };

  this.bootstrap = (node = document.querySelector('[ng-app]')) => {
    $config.forEach(el => el());
    node.querySelectorAll('*').forEach(compile);
  };

  this.directive = directive;
  this.filter = filter;
  this.component = component;
  this.controller = controller;
  this.service = service;
  this.config = config;
  this.constant = constant;
  this.$rootScope = $rootScope;

  service('$http', http);
  service('$timeout', timeout);

  const parseNode = (scope, str) => {
    const [expression, ...filtersArray] = str.split('|').map(str => str.trim());
    const result = scope.eval(expression);

    return filtersArray.reduce((acc, name) => filters[name](acc), result);
  };

  $rootScope.$$parseNode = parseNode;

  setTimeout(this.bootstrap);
}

window.angular = new SmallAngular();
