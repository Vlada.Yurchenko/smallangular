function ngModel() {
  return {
    link: (scope, node, attr) => {
      const varName = node.getAttribute('ng-model');
      const inputProp = attr.type === 'checkbox' ? 'checked' : 'value';

      if (!scope[varName]) {
        scope[varName] = node[inputProp];
      }

      node[inputProp] = scope.eval(varName);

      scope.$whatch(() => varName, node, () => {
        node[inputProp] = scope.eval(varName);
      });

      node.addEventListener('input', ({ target }) => {
        scope.$applyAsync(() => {
          if (inputProp === 'checked') {
            eval(`${varName}= ${target[inputProp]}`);

            return;
          }

          eval(`${varName}= "${target[inputProp]}"`);
        });
      });
    }
  };
}

export default ngModel;