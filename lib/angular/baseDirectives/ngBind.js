function ngBind() {
  return {
    link: (scope, node) => {
      const varName = node.getAttribute('ng-bind');

      const update = () => {
        node.textContent = scope[varName];
      };

      update();
      scope.$whatch(() => varName, node, update);
    }
  };
}

export default ngBind;