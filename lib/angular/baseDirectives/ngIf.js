function ngIf() {
  return {
    link(scope, node) {
      const expression = node.getAttribute('ng-if');

      const update = () => {
        node.hidden = !scope.eval(expression);
      };

      update();
      scope.$whatch(() => expression, node, update);
    }
  };
}

export default ngIf;
