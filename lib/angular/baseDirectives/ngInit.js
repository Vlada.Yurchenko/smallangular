function ngInit() {
  return {
    link: (scope, node) => {
      scope.eval(node.getAttribute('ng-init'));
    }
  };
}

export default ngInit;