function ngRepeat() {
  return {
    link: (scope, node) => {
      const expression = node.getAttribute('ng-repeat');
      const [variable, iterable] = expression.split(/\s*in\s*/);
      scope[variable] = '';
      const { parentNode, style } = node;
      const nodeCopy = node.cloneNode(true);
      style.display = 'none';

      function renderCloneNodes() {
        parentNode.querySelectorAll('[ng-delete]').forEach(el => el.remove());
        const cloneNodesContainer = new DocumentFragment();

        for (const key of scope[iterable]) {
          const clone = nodeCopy.cloneNode(true);
          clone.setAttribute('ng-delete', '');
          clone.removeAttribute('ng-repeat');
          scope[variable] = key;

          clone.innerHTML = clone.innerHTML.replace(/{{(.*?)}}/g, (_, content) => scope.$$parseNode(scope, content));

          cloneNodesContainer.appendChild(clone);
          scope.$$compile(clone);
          clone.querySelectorAll('*').forEach(scope.$$compile);
        }

        parentNode.appendChild(cloneNodesContainer);
      }

      renderCloneNodes();
      scope.$whatch(() => expression, node, renderCloneNodes);
    }
  };
}

export default ngRepeat;
