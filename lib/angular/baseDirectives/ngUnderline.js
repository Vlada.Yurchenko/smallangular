function ngUnderline() {
  return {
    link: (scope, node) => {
      const expression = node.getAttribute('ng-underline');
      const toggleStyle = () => {
        node.style.textDecoration = scope.eval(expression) ? 'underline' : 'none';
      };

      toggleStyle();
      scope.$whatch(() => expression, node, toggleStyle);
    }
  };
}

export default ngUnderline;