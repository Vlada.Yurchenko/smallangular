function ngShow() {
  return {
    link: (scope, node) => {
      const expression = node.getAttribute('ng-show');
      const toggleDisplay = () => {
        node.style.display = scope.eval(expression) ? 'block' : 'none';
      };

      toggleDisplay();
      scope.$whatch(() => expression, node, toggleDisplay);
    }
  };
}

export default ngShow;