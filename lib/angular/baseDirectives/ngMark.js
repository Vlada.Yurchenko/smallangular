function ngMark() {
  return {
    link: (scope, node) => {
      const varName = node.getAttribute('ng-mark');
      const content = node.innerText;
      node.innerText = '';
      const span = document.createElement('span');
      node.appendChild(span);

      function markText() {
        span.innerHTML = '';
        const string = `${scope[varName]}`;
        const newStr = string.replace(/\./g, '\\.');
        const regexp = new RegExp(newStr, 'gi');
        const result = content.replace(regexp, value => `<mark>${value}</mark>`);
        span.innerHTML = result;
      }

      markText();
      scope.$whatch(() => varName, node, markText);
    }
  };
}

export default ngMark;