function ngHide() {
  return {
    link: (scope, node) => {
      const expression = node.getAttribute('ng-hide');
      const toggleDisplay = () => {
        node.style.display = scope.eval(expression) ? 'none' : 'block';
      };

      toggleDisplay();
      scope.$whatch(() => expression, node, toggleDisplay);
    }
  };
}

export default ngHide;