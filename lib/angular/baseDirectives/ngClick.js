function ngClick() {
  return {
    link: (scope, node) => {
      const expression = node.getAttribute('ng-click');

      node.addEventListener('click', () => {
        scope.$applyAsync(() => scope.eval(expression));
      });
    }
  };
}

export default ngClick;