export const checkInjection = fn => (/{\n\s+('inject')/).test(fn.toString());

export const getDependencyNames = args => {
  const fn = args.call ? args : args.pop();

  if (!args.call) {
    return args;
  }

  if (!checkInjection(fn)) {
    return [];
  }

  const argsStr = fn.toString().match(/\((.*?)\)/)[1];

  return argsStr.split(/\s*,\s*/);
};
