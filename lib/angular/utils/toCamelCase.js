const toCamelCase = name => name.toLowerCase().replace(/-./, x => x[1].toUpperCase());

export default toCamelCase;