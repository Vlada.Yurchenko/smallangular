const getHtmlAttributes = attributes =>
  [].reduce.call(attributes, (acc, attr) => {
    if (!(/^ng/).test(attr.name)) {
      acc[attr.name] = attr.value;
    }

    return acc;
  }, {});


const getAllAttributes = ({ attributes }) => {
  const allAttr = [].map.call(attributes, ({ name }) => name);
  const htmlAttr = getHtmlAttributes(attributes);

  return [allAttr, htmlAttr];
};

export default getAllAttributes;