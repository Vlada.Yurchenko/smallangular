function timeout($rootScope) {
  'inject';

  return function(fn, delay) {
    setTimeout(() => $rootScope.$applyAsync(fn), delay);
  };
}

export default timeout;