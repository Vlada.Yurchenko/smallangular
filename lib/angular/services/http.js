function http($rootScope) {
  'inject';

  function request(config) {
    function createRequestUrl(url = '', params) {
      let resultStr = this.baseUrl + url;

      if (!params) {
        return resultStr;
      }

      for (const key in params) {
        const sign = resultStr.includes('?') ? '&' : '?';
        resultStr += `${sign}${key}=${params[key]}`;
      }

      return resultStr;
    }

    const {
      url,
      method,
      params,
      data,
      transformResponse = [],
      responseType = 'json',
      headers,
      onDownloadProgress,
      onUploadProgress
    } = config;

    const fullUrl = createRequestUrl(url, params);

    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();

      xhr.open(method, fullUrl);
      xhr.responseType = responseType;

      const requestHeaders = { ...this.headers, ...headers };

      for (const key in requestHeaders) {
        xhr.setRequestHeader(key, requestHeaders[key]);
      }

      xhr.onload = function() {
        const responceObj = {
          status: xhr.status,
          responceType: xhr.responseType,
          contentType: xhr.getResponseHeader('content-type')
        };

        if (xhr.status >= 400) {
          responceObj.responce = xhr.response;
          reject(responceObj);

          return $rootScope.$applyAsync();
        }

        const transformedData = transformResponse.reduce((acc, cur) => cur(acc), xhr.response);
        responceObj.responce = transformedData ? transformedData : responceObj.responce;
        resolve(responceObj);
        $rootScope.$applyAsync();
      };

      xhr.onprogress = onDownloadProgress;
      xhr.upload.onprogress = onUploadProgress;
      xhr.onerror = () => reject(xhr);

      xhr.send(data || null);
    });
  }

  return {
    get: (fullUrl, config = {}) => request({ ...config, method: 'GET', fullUrl }),
    post: (fullUrl, data, config = {}) => request({ ...config, method: 'POST', fullUrl, data }),
    delete: (fullUrl, config = {}) => request({ ...config, method: 'DELETE', fullUrl })
  };
}

export default http;