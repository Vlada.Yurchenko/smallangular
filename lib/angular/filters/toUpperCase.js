const toUpperCase = value => value.toUpperCase();

export default toUpperCase;