const toCurrencyUS = value =>
  value.replace(/(\d+)(\.\d+)?/, (...det) => `${det[1]}${(det[2] || '.00').slice(0, 3)}$`);

export default toCurrencyUS;
