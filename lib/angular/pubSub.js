class PubSub {
  #events = [];

  subscribe = (name, listener) => {
    if (!this.#events[name]) {
      this.#events[name] = [];
    }

    this.#events[name].push(listener);
  }

  publish = (name, ...data) => {
    if (!this.#events[name]) {
      return [];
    }

    this.#events.forEach(el => el(...data));
  }
}

export default new PubSub();